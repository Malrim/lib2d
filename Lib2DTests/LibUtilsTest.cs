﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Lib2D.Utils;

using Lib2DTests.TestData;

namespace Lib2DTests
{
    [TestClass]
    public class LibUtilsTest
    {
        private List<Item> items;

        [TestInitialize]
        public void Initialize()
        {
            items = new List<Item>();
            items.Add(new Item("Sword"));
            items.Add(new Item("Backpack"));
            items.Add(new Item("Shield"));
        }

        #region IsNameExistsTest

        [TestMethod]
        public void IsNameExistsTest_Sword()
        {
            Assert.IsTrue(LibUtils.IsNameExists("Sword", items));
        }

        [TestMethod]
        public void IsNameExistsTest_Backpack()
        {
            Assert.IsTrue(LibUtils.IsNameExists("BACKPACK", items));
        }

        [TestMethod]
        public void IsNameExistsTest_Shield()
        {
            Assert.IsTrue(LibUtils.IsNameExists("sHiELd", items));
        }

        [TestMethod]
        public void IsNameExistsTest_Space()
        {
            Assert.IsFalse(LibUtils.IsNameExists(" ", items));
        }

        [TestMethod]
        public void IsNameExistsTest_Null()
        {
            Assert.IsFalse(LibUtils.IsNameExists(null, items));
        }

        #endregion
    }
}