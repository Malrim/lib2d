﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Lib2D;

namespace Lib2DTests
{
    [TestClass]
    public class SceneTest
    {
        private GameScene scene;

        [TestInitialize]
        public void Initialize()
        {
            scene = new GameScene("TestScene");

            scene.AddEntity(new Component("Player"), 1);
            scene.AddEntity(new Component("inventory"), 1);
            scene.AddEntity(new Component("enemy"), 1);
        }

        #region GetComponent

        [TestMethod]
        public void GetComponentTest_Player()
        {
            BaseEntity entity = scene.GetEntity("Player");
            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void GetComponentTest_Inventory()
        {
            BaseEntity entity = scene.GetEntity("INVENTORY");
            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void GetComponentTest_Enemy()
        {
            BaseEntity entity = scene.GetEntity("eNeMy");
            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void GetComponentTest_Space()
        {
            BaseEntity entity = scene.GetEntity(" ");
            Assert.IsNull(entity);
        }

        [TestMethod]
        public void GetComponentTest_Null()
        {
            BaseEntity entity = scene.GetEntity(null);
            Assert.IsNull(entity);
        }

        #endregion
    }
}