﻿using Microsoft.Xna.Framework;

namespace Lib2D.ComponentServices
{
    public abstract class ComponentService : ICommon
    {
        public Component Component { get; internal set; }
        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = Component != null ? 
                    Component.Position + value : 
                    value;
            }
        }

        public virtual void Initialize()
        {
        }

        public virtual void Load()
        {
        }

        public abstract void Unload();
    }
}