﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Lib2D.ComponentServices.Graphics
{
    public class Sprite : DrawableComponentService, ISprite
    {
        protected Texture2D Texture;
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public Vector2 Scale { get; set; }
        public float Rotation { get; set; }
        public Vector2 CenterRotation { get; set; }
        public Color ColorSprite { get; set; }
        public SpriteEffects SpriteEffect { get; set; }

        public Sprite(Texture2D texture)
        {
            Texture = texture;

            Width = texture.Width;
            Height = texture.Height;
            Scale = Vector2.One;
            Rotation = 0f;
            CenterRotation = new Vector2(Width / 2, Height / 2);

            ColorSprite = Color.White;
            SpriteEffect = SpriteEffects.None;
        }

        public override void Unload()
        {
            Texture.Dispose();
        }


        public override void Draw()
        {
            Component.Scene.Manager.SpriteBatch.Draw(
                Texture,
                Position,
                null,
                ColorSprite,
                Rotation,
                CenterRotation,
                Scale,
                SpriteEffect,
                0f
                );
        }
    }
}