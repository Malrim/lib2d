﻿namespace Lib2D.ComponentServices.Graphics
{
    public abstract class DrawableComponentService : ComponentService, IDrawable
    {
        public bool Visible { get; set; }

        public DrawableComponentService()
        {
            Visible = true;
        }

        public virtual void Draw()
        {
        }
    }
}