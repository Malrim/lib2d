﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Lib2D.ComponentServices.Graphics
{
    public interface ISprite
    {
        int Width { get; }
        int Height { get; }
        Vector2 Scale { get; set; }
        float Rotation { get; set; }
        Vector2 CenterRotation { get; set; }
        Color ColorSprite { get; set; }
        SpriteEffects SpriteEffect { get; set; }
    }
}