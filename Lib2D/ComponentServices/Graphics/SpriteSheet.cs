﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Lib2D.ComponentServices.Graphics
{
    public class SpriteSheet : ISprite
    {
        protected Texture2D Sheet;
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int Columns { get; private set; }
        public int Rows { get; private set; }
        public int FrameWidth { get; private set; }
        public int FrameHeight { get; private set; }
        public Vector2 Scale { get; set; }
        public float Rotation { get; set; }
        public Vector2 CenterRotation { get; set; }
        public Color ColorSprite { get; set; }
        public SpriteEffects SpriteEffect { get; set; }

        public SpriteSheet(Texture2D spriteSheet, int columns, int rows)
        {
            Sheet = spriteSheet;
            Columns = columns;
            Rows = rows;

            Width = spriteSheet.Width;
            Height = spriteSheet.Height;
            FrameWidth = columns != 0 ? (Width / columns) : 0;
            FrameHeight = rows != 0 ? (Height / rows) : 0;
            Scale = Vector2.One;
            Rotation = 0f;
            CenterRotation = new Vector2(FrameWidth / 2, FrameHeight / 2);

            ColorSprite = Color.White;
            SpriteEffect = SpriteEffects.None;
        }

        public void Unload()
        {
            Sheet.Dispose();
        }

        public void DrawFrame(SpriteBatch spriteBatch, Vector2 position, int column, int row)
        {
            Rectangle sourceRectangle = new Rectangle(
                FrameWidth * column,
                FrameHeight * row,
                FrameWidth,
                FrameHeight
                );

            spriteBatch.Draw(
                Sheet,
                position,
                sourceRectangle,
                ColorSprite,
                Rotation,
                CenterRotation,
                Scale,
                SpriteEffect,
                0f
                );
        }
    }
}