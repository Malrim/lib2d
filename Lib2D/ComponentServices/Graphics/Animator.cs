﻿using Microsoft.Xna.Framework.Graphics;

using System.Collections.Generic;

namespace Lib2D.ComponentServices.Graphics
{
    public class Animator : DrawableComponentService, IUpdateable
    {
        public SpriteSheet SpriteSheet { get; private set; }
        public bool Enabled { get; set; }

        private Dictionary<string, Animation> animations;
        private Animation playingAnimation;

        public Animator(SpriteSheet spriteSheet)
        {
            SpriteSheet = spriteSheet;

            animations = new Dictionary<string, Animation>();
            Enabled = true;
        }

        public override void Unload()
        {
            playingAnimation = null;
            animations.Clear();

            SpriteSheet.Unload();
        }


        public void AddAnimation(Animation animation, string nameAnimation)
        {
            if (animation != null && 
                !animations.ContainsKey(nameAnimation) && 
                !animations.ContainsValue(animation)
                )
            {
                animations.Add(nameAnimation, animation);
            }
        }

        public Animation GetAnimation(string name)
        {
            if (animations.ContainsKey(name))
                return animations[name];

            return null;
        }


        public void PlayAnimation(string name)
        {
            if (playingAnimation != null && playingAnimation.WaitForCompletion)
            {
                if (!playingAnimation.IsCompletion)
                    return;

                playingAnimation.Reset();
            }

            if (animations.ContainsKey(name))
                playingAnimation = animations[name];
        }

        public void StopAnimation()
        {
            // TODO čekat dokud není animace kompletní a poté zakázat update
            if (playingAnimation != null)
                playingAnimation = null;
        }


        public void Update()
        {
            if (playingAnimation != null)
                playingAnimation.UpdateAnimation(Component.Scene.Manager.GameTime);
        }

        public override void Draw()
        {
            if (playingAnimation != null)
            {
                playingAnimation.DrawAnimation(
                    SpriteSheet,
                    Component.Scene.Manager.SpriteBatch,
                    Component.Position
                    );
            }
        }


        public void FlipAnimationHorizontally(bool flip)
        {
            SpriteSheet.SpriteEffect = flip ? SpriteEffects.FlipHorizontally : SpriteEffects.None;
        }

        public void FlipAnimationVertically(bool flip)
        {
            SpriteSheet.SpriteEffect = flip ? SpriteEffects.FlipVertically : SpriteEffects.None;
        }
    }
}