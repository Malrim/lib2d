﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Lib2D.ComponentServices.Graphics
{
    public class Animation
    {
        public float SpeedAnimation { get; set; }
        public bool WaitForCompletion { get; set; }

        private int typeAnimation;
        private int currentFrame;
        private int startFrame;
        private int endFrame;
        private float elapsedTime;

        internal bool IsCompletion { get; private set; }

        public Animation(int typeAnimation, float speedAnimation, bool waitForCompletion, int startFrame, int endFrame)
        {
            this.typeAnimation = typeAnimation;
            SpeedAnimation = speedAnimation;
            this.startFrame = startFrame;
            this.endFrame = endFrame;
            WaitForCompletion = waitForCompletion;
        }


        public void UpdateAnimation(GameTime gameTime)
        {
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapsedTime >= SpeedAnimation)
            {
                currentFrame++;
                IsCompletion = false;

                if (currentFrame > endFrame)
                {
                    currentFrame = startFrame;
                    IsCompletion = true;
                }

                elapsedTime = 0f;
            }
        }

        public void DrawAnimation(SpriteSheet spriteSheet, SpriteBatch spriteBatch, Vector2 position)
        {
            spriteSheet.DrawFrame(spriteBatch, position, currentFrame, typeAnimation);
        }


        internal void Reset()
        {
            currentFrame = startFrame;
            elapsedTime = 0f;
            IsCompletion = false;
        }
    }
}