﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Lib2D.ComponentServices.Graphics;

namespace Lib2D.Components.Map
{
    public class Tile : Sprite
    {
        public int Id { get; private set; }

        private Rectangle rectangleTile;

        public Tile(int id, Texture2D tileset, Rectangle rectangleTile) : base(tileset)
        {
            Id = id;
            this.rectangleTile = rectangleTile;

            Width = rectangleTile.Width;
            Height = rectangleTile.Height;
            CenterRotation = new Vector2(Width / 2, Height / 2);
        }

        public override void Draw()
        {
            Component.Scene.Manager.SpriteBatch.Draw(
                Texture, 
                Position, 
                rectangleTile, 
                ColorSprite, 
                Rotation, 
                CenterRotation, 
                Scale, 
                SpriteEffect, 
                0f
                );
        }
    }
}