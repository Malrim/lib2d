﻿using System.Collections.Generic;

namespace Lib2D.Components.Map
{
    public class TileLayer : Component
    {
        private List<int> tilesData;

        public TileLayer(string name, List<int> tilesData) : base(name)
        {
            this.tilesData = tilesData;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }


        internal override void DrawServices()
        {
        }
    }
}