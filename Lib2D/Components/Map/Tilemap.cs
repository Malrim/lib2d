﻿using System.Collections.Generic;

namespace Lib2D.Components.Map
{
    public class Tilemap : Component
    {
        public string TilesetsDirectory { get; set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int TileWidth { get; private set; }
        public int TileHeight { get; private set; }

        public List<TileLayer> Layers { get; private set; }
        public List<Tileset> Tilesets { get; private set; }

        public Tilemap(string name, string tilesetsDirectory) : base(name)
        {
            TilesetsDirectory = tilesetsDirectory;
            Enabled = false;
            Visible = false;

            Layers = new List<TileLayer>();
            Tilesets = new List<Tileset>();
        }

        protected override void LoadContent()
        {
            for (int i = 0; i < Tilesets.Count; i++)
                Tilesets[i].Load(Scene.Content, TilesetsDirectory);

            for (int i = 0; i < Layers.Count; i++)
                Scene.AddComponent(Layers[i], Layers[i].Order);

            base.LoadContent();
        }

        protected override void UnloadContent()
        {
            for (int i = 0; i < Tilesets.Count; i++)
                Tilesets[i].Unload();
            Tilesets.Clear();

            for (int i = 0; i < Layers.Count; i++)
                Scene.RemoveComponent(Layers[i]);
            Layers.Clear();

            base.UnloadContent();
        }
    }
}