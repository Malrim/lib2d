﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Lib2D.Components.Map
{
    public class Tileset
    {
        public Texture2D Texture { get; private set; }
        public string Name { get; private set; }
        public int Columns { get; private set; }
        public int Rows { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int WidthTile { get; private set; }
        public int HeightTile { get; private set; }
        public int TileCount { get; private set; }
        public int FirstTile { get; private set; }
        public int Spacing { get; private set; }

        public Tileset(
            string name, 
            int columns, 
            int widthTile, 
            int heightTile, 
            int tileCount, 
            int firstTile, 
            int spacing
            )
        {
            Name = name;
            Columns = columns;
            Rows = columns != 0 ? tileCount / columns : 0;
            WidthTile = widthTile;
            HeightTile = heightTile;
            TileCount = tileCount;
            FirstTile = firstTile;
            Spacing = spacing;
        }

        public void Load(ContentManager content, string path)
        {
            Texture = content.Load<Texture2D>(string.Format("{0}/{1}", path, Name));
            Width = Texture.Width;
            Height = Texture.Height;
        }

        public void Unload()
        {
            Texture.Dispose();
        }

        public Rectangle GetTile(int tileId)
        {
            return new Rectangle();
        }
    }
}