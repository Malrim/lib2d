﻿using Microsoft.Xna.Framework.Content;

using System.Collections.Generic;

using Lib2D.Cameras;

namespace Lib2D
{
    public class GameScene : ICommon, IUpdateable, IDrawable
    {
        public GameScenesManager Manager { get; internal set; }
        /// <summary>
        /// Jméno scény
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Povolení aktualizování scény
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// povolení vykreslování scény
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Třída pro práci s herním obsahem
        /// </summary>
        public ContentManager Content { get; private set; }

        public Camera2D Camera { get; set; }

        private List<Component> components;
        private List<Component> updatedComponents;
        private Queue<Component> removedComponents;

        private bool isLoaded;

        public GameScene(string name)
        {
            Name = name;

            Enabled = true;
            Visible = true;

            components = new List<Component>();
            updatedComponents = new List<Component>();
            removedComponents = new Queue<Component>();
        }

        public virtual void Initialize()
        {
        }

        public void Load()
        {
            if (isLoaded) return;
            isLoaded = true;

            Content = new ContentManager(Manager.Game.Content.ServiceProvider, "Content");
            LoadContent();
        }

        protected virtual void LoadContent()
        {
        }

        public void Unload()
        {
            UnloadContent();

            for (int i = 0; i < components.Count; i++)
                components[i].Unload();

            components.Clear();
            updatedComponents.Clear();
            removedComponents.Clear();

            Content.Unload();
            Content.Dispose();
            Content = null;

            isLoaded = false;
        }

        protected virtual void UnloadContent()
        {
        }


        public void AddComponent(Component component, byte order)
        {
            if (component != null && !components.Contains(component))
            {
                int index = GetDrawOrderIndex(order, components);
                components.Insert(index, component);
                component.Scene = this;
                component.Order = order;
                component.Initialize();
                component.Load();
            }
        }

        public Component GetComponent(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                Component component = components.Find(
                    cmp => cmp.Name.ToLower() == name.ToLower()
                    );

                return component;
            }

            return null;
        }

        public void RemoveComponent(Component component)
        {
            if (component != null && components.Contains(component))
                removedComponents.Enqueue(component);
        }


        public void Update()
        {
            updatedComponents.Clear();
            for (int i = 0; i < components.Count; i++)
                updatedComponents.Add(components[i]);

            for (int i = 0; i < updatedComponents.Count; i++)
            {
                if (updatedComponents[i].Enabled)
                {
                    updatedComponents[i].Update();
                    updatedComponents[i].UpdateServices();
                }
            }
        }

        public void RemovalUpdate()
        {
            while (removedComponents.Count > 0)
            {
                Component component = removedComponents.Dequeue();
                components.Remove(component);

                component.OnRemoved();
                component.Unload();
                component.Scene = null;
            }
        }

        public void Draw()
        {
            for (int i = 0; i < components.Count; i++)
            {
                if (components[i].Visible)
                {
                    components[i].Draw();
                    components[i].DrawServices();
                }
            }
        }


        private int GetDrawOrderIndex(byte drawOrder, List<Component> components)
        {
            Component component = components.Find(cmp => cmp.Order == drawOrder);

            if (component == null)
            {
                int index = components.FindIndex(cmp => cmp.Order >= drawOrder);
                if (index == -1)
                    return components.Count;
                else return index;
            }

            return components.FindLastIndex(cmp => cmp.Order == drawOrder) + 1;
        }
    }
}