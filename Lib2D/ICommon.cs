﻿namespace Lib2D
{
    public interface ICommon
    {
        void Initialize();
        void Load();
        void Unload();
    }
}