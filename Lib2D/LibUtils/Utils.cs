﻿using System.Collections.Generic;

namespace Lib2D.LibUtils
{
    public static class Utils
    {
        public static bool IsNameExists<T>(string name, List<T> list)
        {
            if (!string.IsNullOrEmpty(name))
            {
                int index = list.FindIndex(
                    x =>
                    typeof(T).GetProperty("Name").GetValue(x).ToString().ToLower() == name.ToLower()
                    );

                return index >= 0;
            }

            return false;
        }
    }
}