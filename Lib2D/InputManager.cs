﻿using Microsoft.Xna.Framework.Input;

namespace Lib2D
{
    public class InputManager
    {
        /// <summary>
        /// Aktuální stav klávesy
        /// </summary>
        private KeyboardState currentKey;
        /// <summary>
        /// Předešlý stav klávesy
        /// </summary>
        private KeyboardState lastKey;

        /// <summary>
        /// Aktuální stav myši
        /// </summary>
        private MouseState currentMouseState;
        /// <summary>
        /// Předešlý stav myši
        /// </summary>
        private MouseState lastMouseState;

        /// <summary>
        /// Metoda aktualizuje uživatelské vstupy
        /// </summary>
        public void Update()
        {
            lastKey = currentKey;
            currentKey = Keyboard.GetState();

            lastMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();
        }

        public bool PressedKey(Keys key)
        {
            return (currentKey.IsKeyDown(key) && lastKey.IsKeyUp(key));
        }

        public bool HoldKey(Keys key)
        {
            return (currentKey.IsKeyDown(key) && lastKey.IsKeyDown(key));
        }

        public bool ReleasedKey(Keys key)
        {
            return (currentKey.IsKeyUp(key) && lastKey.IsKeyDown(key));
        }


        public bool RightPressedButton()
        {
            return (currentMouseState.RightButton == ButtonState.Pressed && lastMouseState.RightButton == ButtonState.Released);
        }

        public bool RightHoldButton()
        {
            return (currentMouseState.RightButton == ButtonState.Pressed && lastMouseState.RightButton == ButtonState.Pressed);
        }

        public bool RightReleasedButton()
        {
            return (currentMouseState.RightButton == ButtonState.Released && lastMouseState.RightButton == ButtonState.Pressed);
        }


        public bool MiddlePressedButton()
        {
            return (currentMouseState.MiddleButton == ButtonState.Pressed && lastMouseState.MiddleButton == ButtonState.Released);
        }

        public bool MiddleHoldButton()
        {
            return (currentMouseState.MiddleButton == ButtonState.Pressed && lastMouseState.MiddleButton == ButtonState.Pressed);
        }

        public bool MiddleReleasedButton()
        {
            return (currentMouseState.MiddleButton == ButtonState.Released && lastMouseState.MiddleButton == ButtonState.Pressed);
        }


        public bool LeftPressedButton()
        {
            return (currentMouseState.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Released);
        }

        public bool LeftHoldButton()
        {
            return (currentMouseState.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Pressed);
        }

        public bool LeftReleasedButton()
        {
            return (currentMouseState.LeftButton == ButtonState.Released && lastMouseState.LeftButton == ButtonState.Pressed);
        }
    }
}