﻿using Microsoft.Xna.Framework.Graphics;

namespace Lib2D
{
    public interface IGraphics
    {
        SpriteSortMode SortMode { get; set; }
        BlendState BlendState { get; set; }
        SamplerState SamplerState { get; set; }
    }
}