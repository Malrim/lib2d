﻿namespace Lib2D
{
    public interface IDrawable
    {
        bool Visible { get; set; }
        void Draw();
    }
}