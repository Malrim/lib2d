﻿namespace Lib2D
{
    public interface IUpdateable
    {
        bool Enabled { get; set; }
        void Update();
    }
}