﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Collections.Generic;

namespace Lib2D
{
    public sealed class GameScenesManager
    {
        public Game Game { get; private set; }
        public GameTime GameTime { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }
        public InputManager Input { get; private set; }

        private Stack<GameScene> gameScenes;
        private List<GameScene> updatingGameScenes;

        public GameScenesManager(Game game, SpriteBatch spriteBatch)
        {
            Game = game;
            SpriteBatch = spriteBatch;

            Input = new InputManager();
            gameScenes = new Stack<GameScene>();
            updatingGameScenes = new List<GameScene>();
        }

        public void UnloadContent()
        {
            while(gameScenes.Count > 0)
            {
                GameScene scene = gameScenes.Pop();
                scene.Unload();
                scene.Manager = null;
            }
        }


        public void PushGameScene(GameScene scene)
        {
            if (scene != null && !gameScenes.Contains(scene))
            {
                gameScenes.Push(scene);
                scene.Manager = this;
                scene.Initialize();
                scene.Load();
            }
        }

        public GameScene PeekGameScene()
        {
            if (gameScenes.Count > 0)
                return gameScenes.Peek();

            return null;
        }

        public GameScene PopGameScene()
        {
            if (gameScenes.Count > 0)
            {
                GameScene scene = gameScenes.Pop();
                scene.Unload();
                scene.Manager = null;

                return scene;
            }

            return null;
        }


        public void UpdateGameScenes(GameTime gameTime)
        {
            GameTime = gameTime;
            Input.Update();

            updatingGameScenes.Clear();
            foreach (GameScene scene in gameScenes)
                updatingGameScenes.Add(scene);

            foreach (GameScene scene in updatingGameScenes)
            {
                if (scene.Enabled)
                {
                    scene.Update();
                    scene.RemovalUpdate();
                }
            }
        }

        public void DrawGameScenes(GameTime gameTime)
        {
            GameTime = gameTime;

            foreach (GameScene scene in gameScenes)
                if (scene.Visible) scene.Draw();
        }
    }
}