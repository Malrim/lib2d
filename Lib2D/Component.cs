﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Collections.Generic;

using Lib2D.ComponentServices;

namespace Lib2D
{
    public class Component : ICommon, IUpdateable, IDrawable, IGraphics
    {
        public GameScene Scene { get; internal set; }
        public string Name { get; private set; }
        public byte Order { get; internal set; }
        public bool Enabled { get; set; }
        public bool Visible { get; set; }

        public Vector2 Position { get; set; }
        public SpriteSortMode SortMode { get; set; }
        public BlendState BlendState { get; set; }
        public SamplerState SamplerState { get; set; }

        private List<ComponentService> componentServices;
        private bool isLoaded;

        public Component(string name)
        {
            Enabled = true;
            Visible = true;

            SortMode = SpriteSortMode.Deferred;
            BlendState = BlendState.AlphaBlend;
            componentServices = new List<ComponentService>();
        }

        public virtual void Initialize()
        {
        }

        public void Load()
        {
            if (isLoaded) return;
            isLoaded = true;

            LoadContent();
        }

        protected virtual void LoadContent()
        {
        }

        public virtual void OnRemoved()
        {
        }

        public void Unload()
        {
            UnloadContent();

            for (int i = 0; i < componentServices.Count; i++)
            {
                componentServices[i].Unload();
                componentServices[i].Component = null;
            }
            componentServices.Clear();

            isLoaded = false;
        }

        protected virtual void UnloadContent()
        {
        }


        public void AddComponentService(ComponentService service)
        {
            if (service != null && !componentServices.Contains(service))
            {
                componentServices.Add(service);
                service.Component = this;
                service.Initialize();
                service.Load();
            }
        }

        public ComponentService GetComponentService<T>()
        {
            ComponentService service = componentServices.Find(
                s => s.Equals(typeof(T))
                );

            return service;
        }


        internal virtual void UpdateServices()
        {
            for (int i = 0; i < componentServices.Count; i++)
            {
                if (componentServices[i] is IUpdateable)
                {
                    IUpdateable service = componentServices[i] as IUpdateable;
                    if (service.Enabled)
                    {
                        service.Update();
                    }
                }
            }
        }

        public virtual void Update()
        {
        }

        internal virtual void DrawServices()
        {
            if (componentServices.Count == 0)
                return;

            Matrix? transform = null;
            if (Scene.Camera != null)
                transform = Scene.Camera.Transform;

            Scene.Manager.SpriteBatch.Begin(
                SortMode, 
                BlendState, 
                SamplerState, 
                null, 
                null, 
                null, 
                transform
                );
            for (int i = 0; i < componentServices.Count; i++)
            {
                if (componentServices[i] is IDrawable)
                {
                    IDrawable service = componentServices[i] as IDrawable;
                    if (service.Visible)
                    {
                        service.Draw();
                    }
                }
            }
            Scene.Manager.SpriteBatch.End();
        }

        public virtual void Draw()
        {
        }
    }
}